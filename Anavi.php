class Anavi
{
    var $rules = [
        'o' => '0',
        'l' => '1',
        'm' => 'n',
    ];

    var $aList = [];
    var $anaviEscribio = "";
    var $profesorEscribio = "";
    var $memory = [];



    function isMatch($anaviEscribio, $profesorEscribio)
    {
        if(isset($this->memory[$anaviEscribio])){
            return $this->memory[$anaviEscribio];
        }
        $PosivilidadProfesorEscribio = 'Improbable';
        if ($this->validate($anaviEscribio, $profesorEscribio)) {
            $currentString = $profesorEscribio;
            $this->anaviEscribio = $anaviEscribio;
            $this->profesorEscribio = $profesorEscribio;

            $aPositions = $this->getPositions($currentString);

            foreach ($aPositions as $pos => $leter) {
                if ($this->permutar($currentString, $pos, $aPositions)) {
                    $PosivilidadProfesorEscribio = 'Probable';
                    break;
                }
            }
        }
        $this->memory[$anaviEscribio] = $PosivilidadProfesorEscribio;
        echo $PosivilidadProfesorEscribio;
    }

    function validate($anaviEscribio, $profesorEscribio){
        if(strlen($anaviEscribio) <= 50){
            if(strlen($anaviEscribio) == strlen($profesorEscribio)){
                $pattern = "/[a-zA-Z0-9[:space:]]*$/";
                if(preg_match ($pattern, $anaviEscribio) && preg_match ($pattern, $profesorEscribio)){
                    return true;
                }else{
                    //echo "Las cadenas no cumplen los estandares - ";
                }
            }else{
                //echo "El tama�o de las cadenas no son iguales - ";
            }
        }else{
            //echo "Tama�o de cadena incorrecto - ";
        }
        return false;
    }

    private function permutar($currentString, $pos, $aPositions)
    {
        $leter = substr($currentString, $pos, 1);
        $rule = $this->getRule($leter);
        foreach ($rule as $ruleLetter) {
            $currentString = substr_replace($currentString, $ruleLetter, $pos, 1);
            $this->aList[] = $currentString;
            if ($currentString == $this->anaviEscribio) {
                return true;
            }

            foreach ($aPositions as $pos2 => $let2) {
                if ($pos2 > $pos) {
                    if ($this->permutar($currentString, $pos2, $aPositions)) {
                        return true;
                    }
                }
            }
        }

        return false;

    }

    private function getRule($leter)
    {
        foreach ($this->rules as $id => $val) {
            if ($leter == $id || $leter == $val) {
                return [$id, $val];
            }
        }
        return [];
    }

    private function getPositions($currentString)
    {
        $aPositions = [];
        for ($i = 0; $i < strlen($currentString); $i++) {
            $leter = substr($currentString, $i, 1);
            if ($this->isExisteLetter($leter)) {
                $aPositions[$i] = $leter;
            }
        }
        return $aPositions;
    }

    private function isExisteLetter($leter)
    {
        foreach ($this->rules as $id => $val) {
            if ($leter === $val || $leter === $id) {
                return true;
            }
        }
        return false;
    }
}